Te invitamos a poner a prueba tus aprendizajes adquiridos hasta ahora en el curso. Se trata del producto resultante de llevar a cabo las guías prácticas del Módulo 2:

a. Construir elementos de navegación en un sitio web.

b. Mostrar información utilizando tablas, tarjetas y media.

c. Construir formularios y botones para interacturar con los usuarios

En tu proyecto deberá verse:

1. la barra de navegación queda fija en la parte superior, es decir, que por más que te deslices hacia abajo, la barra siempre permanece visible al tope de la página.
2. la versión responsive de la barra que se ve bien en los diferentes dispositivos.
3. los componentes breadcrumbs en las diferentes páginas, vinculando a las páginas correspondientes y ubicando al usuario en el mapa de sitio que corresponde.
4. un formulario para dejar un comentario, entrar en contacto con el sitio o suscribirse a algún servicio o compra que se ofrezca en el sitio. Los labels agrupados con los controles correspondientes, y manteniendo la alineación y ubicación de los elementos de forma armónica. El formulario incluye campos de: email, de listado seleccionable, checkbox, texto y textarea.
5. la librería open-iconic y algunos iconos en la tabla que ilustran la información
6. el contenido de la tabla que puede verse con scroll sin deformar la página.
7. En la página principal, el listado de productos utiliza el componente card con los títulos, cuerpo y footer.
8. la disposición de los elementos (márgenes y padding, atributos de flex, etc.) es prolija y moderna.
9. una imagen en cada una de las cards que representen el artículo que estás ofreciendo.
10. una imagen en la página principal a modo de banner o encabezado que haga atractivo el sitio o muestre una promoción. Utiliza los componentes adecuados para este propósito.

Recuerda que esta actividad será evaluada por tus pares y se espera que también lo hagas tú. Por ello, es muy importante que evalúes a conciencia, pensando en que tantos tus compañeros como vos, están queriendo aprender en este curso. Guíate por los criterios de corrección que te orientarán en todo el proceso de evaluación.

¡Éxitos!